import { useEffect } from 'react'

import { MessageTypes, useMessages } from 'entities/message'
import { useSession } from 'entities/session'

import { api, EventTypes } from 'shared/api'

export const useInitChat = () => {
  const { initSession } = useSession()
  const { addMessage } = useMessages()

  useEffect(() => {
    initSession()
      .then((cuid) => {
        if (cuid) {
          return api.requestEvent({ cuid, euid: EventTypes.READY })
        }
      })
      .then((res) => {
        addMessage({
          // Дефолтный ответ, если сервер вернул пустую строку
          text: res?.data.result.text.value || 'Здраствуйте',
          type: MessageTypes.RECEIVED,
        })
      })
  }, [])
}
