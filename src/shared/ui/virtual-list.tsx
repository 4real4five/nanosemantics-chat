import type { CSSProperties, ReactNode } from 'react'
import { useCallback, useEffect, useRef } from 'react'
import AutoSizer from 'react-virtualized-auto-sizer'
import { VariableSizeList as List } from 'react-window'

interface VirtualListProps<T> {
  items: T[]
  renderItem: (item: T) => ReactNode
  gap?: number
}

interface RowProps<T> {
  style: CSSProperties
  data: T[]
  index: number
}

export const VirtualList = <T,>(props: VirtualListProps<T>) => {
  const { items, renderItem, gap = 8 } = props
  const listRef = useRef<any>(null)
  const rowHeights = useRef<Record<string, number>>({})

  const setRowHeight = (index: number, size: number) => {
    listRef.current.resetAfterIndex(0)
    rowHeights.current = { ...rowHeights.current, [index]: size }
  }

  const scrollToBottom = () => {
    listRef.current?.scrollToItem(items.length - 1, 'end')
  }

  const getRowHeight = (index: number) => {
    return rowHeights.current[index] + gap || 50
  }

  useEffect(() => {
    if (items.length > 0) {
      scrollToBottom()
    }
  }, [items])

  const Row = useCallback(
    (props: RowProps<T>) => {
      const { data, index, style } = props
      const item = data[index]

      const rowRef = useRef<HTMLDivElement>(null)

      useEffect(() => {
        if (rowRef.current) {
          setRowHeight(index, rowRef.current.clientHeight)
        }
      }, [rowRef])

      return (
        <div style={style}>
          <div ref={rowRef}>{renderItem(item)}</div>
        </div>
      )
    },
    [renderItem]
  )

  return (
    <AutoSizer>
      {({ height, width }) => (
        <List
          style={{ scrollBehavior: 'smooth' }}
          ref={listRef}
          width={width || 0}
          height={height || 0}
          itemCount={items.length}
          itemSize={getRowHeight}
          itemData={items}>
          {Row}
        </List>
      )}
    </AutoSizer>
  )
}
