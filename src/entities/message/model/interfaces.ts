export enum MessageTypes {
  RECEIVED = 'received',
  SENT = 'SENT',
}

export interface IMessage {
  text: string
  type: MessageTypes
}
