import type webpack from 'webpack'

import { buildDevServer } from './build-dev-server'
import { buildLoaders } from './build-loaders'
import { buildPlugins } from './build-plugins'
import { buildResolvers } from './build-resolvers'
import type { IBuildOptions } from './types/config'

export function buildWebpackConfig(options: IBuildOptions): webpack.Configuration {
  const { mode, paths } = options

  return {
    mode,
    entry: paths.entry,
    output: {
      path: paths.build,
      filename: '[name].[contenthash].js',
      clean: true,
    },
    plugins: buildPlugins(options),
    resolve: buildResolvers(options),
    devServer: options.isDev ? buildDevServer(options) : undefined,
    devtool: options.isDev ? 'inline-source-map' : undefined,
    module: {
      rules: buildLoaders(options),
    },
  }
}
