import type { SetStateAction } from 'react'
import { useState } from 'react'

import { isFunction } from './isFunction'
import { useEvent } from './useEvent'

// функция для поддержки работы JSON.parse с undefined
const parseJSON = <T>(value: string | null): T | undefined => {
  try {
    return value === 'undefined' ? undefined : JSON.parse(value ?? '')
  } catch {
    if (__IS_DEV__) {
      // eslint-disable-next-line no-console
      console.log('parsing error on', { value })
    }
    return undefined
  }
}
export const useLocalStorage = <Value>(key: string, initialValue: Value | (() => Value)) => {
  const [value, setValue] = useState<Value>(() => {
    try {
      const item = window.localStorage.getItem(key)

      const actualInitialValue = isFunction(initialValue) ? initialValue() : initialValue

      if (item) {
        return parseJSON<Value>(item) ?? actualInitialValue
      }

      return actualInitialValue
    } catch (error) {
      if (__IS_DEV__) {
        // eslint-disable-next-line no-console
        console.warn(`Error reading localStorage key “${key}”:`, error)
      }
      return isFunction(initialValue) ? initialValue() : initialValue
    }
  })

  const setState = useEvent((newValue: SetStateAction<Value>) => {
    try {
      const actualNewValue = newValue instanceof Function ? newValue(value) : newValue

      localStorage.setItem(key, JSON.stringify(actualNewValue))

      setValue(actualNewValue)
    } catch (error) {
      if (__IS_DEV__) {
        // eslint-disable-next-line no-console
        console.warn(`Error writing localStorage key “${key}”:`, error)
      }
    }
  })

  return [value, setState] as const
}
