import { Button } from '@mui/material'

import { AlertDialog } from 'shared/ui/aleart-dialog'

import { useResetChat } from './model'

export const ResetChatButton = () => {
  const resetSession = useResetChat()
  return (
    <AlertDialog
      title="Удалить историю чата?"
      description="Вы действительно хотите удалить историю чата? Это действие невозможно отменить"
      agreeText="Да"
      disagreeText="Нет"
      onAgree={resetSession}
      trigger={(openDialog) => (
        <Button
          onClick={openDialog}
          color="error">
          Сбросить
        </Button>
      )}
    />
  )
}
