import { useCallback, useContext } from 'react'

import { api } from 'shared/api'
import { UUID } from 'shared/config/env'

import { SessionContext } from './context'

export const useSession = () => {
  const { cuid, setCuid } = useContext(SessionContext)

  const resetSession = useCallback(() => {
    setCuid('')
  }, [])

  const initSession = async (): Promise<string> => {
    try {
      const initRes = await api.initChat({ uuid: UUID, cuid })

      setCuid(initRes.data.result.cuid)

      return initRes.data.result.cuid
    } catch (e) {
      if (__IS_DEV__) {
        // eslint-disable-next-line no-console
        console.log(e)
      }
      throw e
    }
  }

  return { cuid, resetSession, setCuid, initSession }
}
