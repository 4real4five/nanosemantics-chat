import { useCallback, useLayoutEffect, useRef } from 'react'

// возвращает актуальную функцию, при этом не меняя ссылку функции
export const useEvent = <T extends (...args: any[]) => any>(callback: T) => {
  const callbackRef = useRef(callback)

  useLayoutEffect(() => {
    callbackRef.current = callback
  }, [callback])

  return useCallback(
    (...args: Parameters<T>): ReturnType<T> => {
      return callbackRef.current.apply(null, args)
    },
    [callbackRef]
  )
}
