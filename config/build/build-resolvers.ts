import TsconfigPathsPlugin from 'tsconfig-paths-webpack-plugin'
import type webpack from 'webpack'

import type { IBuildOptions } from './types/config'

export function buildResolvers({ paths }: IBuildOptions): webpack.ResolveOptions {
  return {
    extensions: ['.tsx', '.ts', '.js'],
    preferAbsolute: true,
    mainFiles: ['index'],
    modules: [paths.src, 'node_modules'],
    plugins: [new TsconfigPathsPlugin()],
  }
}
