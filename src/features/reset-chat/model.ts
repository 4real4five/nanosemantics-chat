import { useMessages } from 'entities/message'
import { useSession } from 'entities/session'

export const useResetChat = () => {
  const { resetMessages } = useMessages()
  const { resetSession } = useSession()

  return () => {
    resetSession()
    resetMessages()
  }
}
