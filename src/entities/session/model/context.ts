import { createContext } from 'react'

interface SessionContextState {
  cuid?: string
  setCuid: (cuid: string) => void
}

export const SessionContext = createContext<SessionContextState>({ setCuid: () => {} })
