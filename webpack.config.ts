// noinspection ES6PreferShortImport

import path from 'path'

import type webpack from 'webpack'

import type { BuildMode, IBuildEnv, IBuildPaths } from './config/build'
import { buildWebpackConfig } from './config/build'

// eslint-disable-next-line import/no-default-export
export default (env: IBuildEnv): webpack.Configuration => {
  const paths: IBuildPaths = {
    entry: path.resolve(__dirname, 'src', 'app', 'index.tsx'),
    build: path.resolve(__dirname, 'build'),
    html: path.resolve(__dirname, 'public', 'index.html'),
    src: path.resolve(__dirname, 'src'),
  }

  const mode: BuildMode = env.mode || 'development'
  const isDev = mode === 'development'
  const PORT = env.port || 3000

  return buildWebpackConfig({
    mode,
    paths,
    isDev,
    port: PORT,
  })
}
