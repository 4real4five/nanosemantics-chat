import { useCallback } from 'react'
import type { ComponentPropsWithoutRef } from 'react'

import { VirtualList } from 'shared/ui/virtual-list'

import type { IMessage } from '../model'

import { Message } from './message'

interface VirtualListProps {
  messages: IMessage[]
}

export const VirtualMessageList = (props: VirtualListProps) => {
  const { messages } = props

  const renderItem = useCallback(
    ({ text, type }: ComponentPropsWithoutRef<typeof Message>) => (
      <Message
        text={text}
        type={type}
      />
    ),
    []
  )

  return (
    <VirtualList
      items={messages}
      renderItem={renderItem}
    />
  )
}
