export interface InitChatArgs {
  uuid: string
  cuid?: string
  context?: object
}

export interface InitChatData {
  id: number
  result: {
    cuid: string // Идентификатор чата.
    inf: {
      name: string // Имя инфа.
    }
    events: {}
  }
}

export interface RequestChatArgs {
  cuid: string // Идентификатор чата.
  text: string // Текст запроса, UTF-8.
  context?: object // Контекст запроса. Необязательный параметр.
}

export interface RequestChatData {
  id: number
  result: {
    text: {
      value: string // Ответ инфа.
      delay: number // Необязательный. Используется в спец. проектах. "status": <int> // Необязательный. Используется в спец. проектах.
    }
    animation: {
      // Необязательный. Используется в спец. проектах.
      type: number
      duration: number
      isFaded: number
    }

    navigate: {
      // Необязательный. Используется в спец. проектах.
      url: string
    }
    token: string // Не используется.
    showExpSys: string // Не используется.
    rubric: string // Рубрика ответа. Используется в спец. проектах.
    cuid: string // Идентификатор чата.
    context: object // Контекст ответа. Необязательный.
    id: string // Идентификатор ответа.
  }
}

export enum EventTypes {
  READY = '00b2fcbe-f27f-437b-a0d5-91072d840ed3',
}

export interface RequestEventArgs {
  cuid: string // Идентификатор чата.
  euid: EventTypes // Идентификатор события.
  context?: object // Контекст запроса. Необязательный параметр.
}
