import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import DialogTitle from '@mui/material/DialogTitle'
import type { ReactNode } from 'react'
import { useState } from 'react'

interface AlertDialogProps {
  trigger: (openDialog: () => void) => ReactNode
  title?: string
  description?: string
  agreeText?: string
  disagreeText?: string
  onAgree?: () => void
}

export const AlertDialog = (props: AlertDialogProps) => {
  const { trigger, description, disagreeText, agreeText, title, onAgree } = props
  const [open, setOpen] = useState(false)

  const handleClickOpen = () => {
    setOpen(true)
  }

  const handleClose = () => {
    setOpen(false)
  }

  const handleAgree = () => {
    onAgree?.()
    handleClose()
  }

  return (
    <div>
      {trigger(handleClickOpen)}
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">{description}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            color="error"
            onClick={handleClose}>
            {disagreeText}
          </Button>
          <Button
            onClick={handleAgree}
            autoFocus>
            {agreeText}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}
