import { useState } from 'react'

import { MessageTypes, useMessages } from 'entities/message'
import { useSession } from 'entities/session'

import { api } from 'shared/api'

export const useSendMessage = () => {
  const [message, setMessage] = useState('')
  const { cuid, initSession } = useSession()
  const [isLoading, setIsLoading] = useState(false)
  const { addMessage } = useMessages()
  const sendMessage = async () => {
    if (!message) {
      return
    }

    setMessage('')
    addMessage({ text: message, type: MessageTypes.SENT })

    try {
      setIsLoading(true)

      let actualCuid = cuid

      // если пользователь сбросил сессию, то нужно сначало открыть новую
      if (!actualCuid) {
        actualCuid = await initSession()
      }

      const res = await api.requestChat({ cuid: actualCuid, text: message })

      // дефолтный ответ, если сервер вернул пустую строку
      addMessage({
        text: res.data.result.text.value || 'Пустой ответ',
        type: MessageTypes.RECEIVED,
      })
    } catch (e) {
      if (__IS_DEV__) {
        // eslint-disable-next-line no-console
        console.log(e)
      }
    } finally {
      setIsLoading(false)
    }
  }

  return { message, setMessage, sendMessage, isLoading }
}
