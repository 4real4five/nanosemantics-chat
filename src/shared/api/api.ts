import axios from 'axios'

import type {
  InitChatArgs,
  InitChatData,
  RequestChatArgs,
  RequestChatData,
  RequestEventArgs,
} from 'shared/api/interfaces'
import { API_URL } from 'shared/config/env'

const instance = axios.create({
  baseURL: API_URL,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
})

export const initChat = async (data: InitChatArgs) => {
  return instance.post<InitChatData>('/Chat.init', data)
}

export const requestChat = async (data: RequestChatArgs) => {
  return instance.post<RequestChatData>('/Chat.request', data)
}

export const requestEvent = async (data: RequestEventArgs) => {
  return instance.post<RequestChatData>('/Chat.event', data)
}
