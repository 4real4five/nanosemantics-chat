import type { PropsWithChildren } from 'react'

import { useLocalStorage } from 'shared/lib/useLocalStorage'

import { SessionContext } from './context'

export const SessionProvider = ({ children }: PropsWithChildren<unknown>) => {
  const [cuid, setCuid] = useLocalStorage('cuid', '')

  // eslint-disable-next-line react/jsx-no-constructed-context-values
  return <SessionContext.Provider value={{ cuid, setCuid }}>{children}</SessionContext.Provider>
}
