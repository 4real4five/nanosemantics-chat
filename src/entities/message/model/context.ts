import type { SetStateAction } from 'react'
import { createContext } from 'react'

import type { IMessage } from './interfaces'

export interface MessagesContextState {
  messages: IMessage[]
  setMessages: (messages: SetStateAction<IMessage[]>) => void
}

export const MessagesContext = createContext<MessagesContextState>({
  messages: [],
  setMessages: () => {},
})
