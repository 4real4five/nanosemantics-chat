const getEnvVar = (key: string) => {
  if (process.env[key] === undefined) {
    throw new Error(`Env variable ${key} is required`)
  }
  return process.env[key] || ''
}

export const UUID = getEnvVar('UUID')
export const API_URL = getEnvVar('API_URL')
