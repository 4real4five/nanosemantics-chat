import { Button, CircularProgress, TextField } from '@mui/material'

import { useSendMessage } from './model'

export const SendMessage = () => {
  const { sendMessage, message, setMessage, isLoading } = useSendMessage()
  return (
    <>
      <TextField
        value={message}
        onChange={(e) => setMessage(e.target.value)}
        multiline
        variant="outlined"
        label="Введите сообщение"
        maxRows={3}
        sx={{ flexGrow: '1', minWidth: 200 }}
      />
      <Button
        endIcon={isLoading ? <CircularProgress size={16} /> : undefined}
        disabled={isLoading}
        onClick={sendMessage}
        variant="outlined"
        sx={{ height: 'fit-content', flexShrink: '0', px: 2, borderRadius: 2 }}>
        Отправить
      </Button>
    </>
  )
}
