import { useMemo } from 'react'
import type { PropsWithChildren } from 'react'

import { useLocalStorage } from 'shared/lib/useLocalStorage'

import type { MessagesContextState } from './context'
import { MessagesContext } from './context'
import type { IMessage } from './interfaces'

export const MessageProvider = ({ children }: PropsWithChildren<unknown>) => {
  const [messages, setMessages] = useLocalStorage<IMessage[]>('messages', [])

  const defaultState: MessagesContextState = useMemo(
    () => ({ messages: messages ?? [], setMessages }),
    [messages]
  )

  return <MessagesContext.Provider value={defaultState}>{children}</MessagesContext.Provider>
}
