import { Box, Typography } from '@mui/material'

import { ResetChatButton } from 'features/reset-chat'
import { SendMessage } from 'features/send-message'

import { useMessages, VirtualMessageList } from 'entities/message'

import { useInitChat } from './model'

export const ChatPage = () => {
  const { messages } = useMessages()
  useInitChat()

  return (
    <Box
      display="flex"
      flexDirection="column"
      justifyContent="end"
      alignItems="center"
      bgcolor="white"
      height="100%"
      borderRadius={4}
      p={4}
      component="section">
      <Box
        overflow="hidden"
        height="100%"
        width="100%"
        mb={2}>
        {messages.length > 0 ? (
          <VirtualMessageList messages={messages} />
        ) : (
          <Box
            height="100%"
            width="100%"
            display="flex"
            justifyContent="center"
            alignItems="center">
            <Typography variant="h4">История чата пуста</Typography>
          </Box>
        )}
      </Box>
      <Box
        gap={4}
        display="flex"
        flexWrap="wrap"
        alignItems="center"
        width="100%">
        <SendMessage />
        <ResetChatButton />
      </Box>
    </Box>
  )
}
