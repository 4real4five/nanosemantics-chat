import { Box, Button, Typography } from '@mui/material'
import type { ErrorInfo, ReactNode } from 'react'
import { Component } from 'react'

interface Props {
  children?: ReactNode
}

interface State {
  hasError: boolean
}
export class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props)
    this.state = { hasError: false }
  }

  static getDerivedStateFromError() {
    return { hasError: true }
  }

  componentDidCatch(error: Error, info: ErrorInfo) {
    if (__IS_DEV__) {
      // eslint-disable-next-line no-console
      console.log(error, info)
    }
  }

  public render() {
    const { hasError } = this.state
    const { children } = this.props

    if (hasError) {
      return (
        <Box
          height="100vh"
          width="100vw"
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center">
          <Typography
            mb={2}
            variant="h5">
            Произошла ошибка, попробуйте перезагрузить страницу
          </Typography>
          <Button>Перезагрузить</Button>
        </Box>
      )
    }

    return children
  }
}
