import { Box, Container } from '@mui/material'

import { ChatPage } from 'pages/chat'

import { MessageProvider } from 'entities/message'
import { SessionProvider } from 'entities/session'

import { ErrorBoundary } from 'shared/ui/error-boundary'

export const App = () => {
  return (
    <ErrorBoundary>
      <SessionProvider>
        <MessageProvider>
          <Box
            bgcolor="primary.light"
            height="100%"
            p={4}>
            <Container
              maxWidth="md"
              sx={{ height: '100%' }}>
              <ChatPage />
            </Container>
          </Box>
        </MessageProvider>
      </SessionProvider>
    </ErrorBoundary>
  )
}
