import { alpha, Box, Typography, useTheme } from '@mui/material'

import type { IMessage } from '../model'
import { MessageTypes } from '../model'

interface MessageProps extends IMessage {}
export const Message = (props: MessageProps) => {
  const { type, text } = props
  const { palette } = useTheme()

  const isSent = type === MessageTypes.SENT

  return (
    <Box
      bgcolor={alpha(isSent ? palette.primary.light : palette.secondary.light, 0.1)}
      py={1}
      px={3}
      width="70%"
      ml={isSent ? 'auto' : 0}
      mr={isSent ? 0 : 'auto'}
      borderRadius={2}>
      <Typography
        display="block"
        sx={{ wordBreak: 'break-all' }}>
        {text}
      </Typography>
    </Box>
  )
}
