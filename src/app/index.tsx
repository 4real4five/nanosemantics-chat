import { createRoot } from 'react-dom/client'

import { App } from './app'

import 'shared/ui/styles/global.css'

const root = createRoot(document.getElementById('root')!)
root.render(<App />)
