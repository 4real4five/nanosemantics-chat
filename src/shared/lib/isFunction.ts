export const isFunction = (value: unknown): value is Function => {
  return Boolean(value) && value instanceof Function
}
