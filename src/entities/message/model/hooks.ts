import { useContext } from 'react'

import { MessagesContext } from './context'
import type { IMessage } from './interfaces'

export const useMessages = () => {
  const { messages, setMessages } = useContext(MessagesContext)

  const addMessage = (message: IMessage) => {
    setMessages((prev) => [...prev, message])
  }

  const resetMessages = () => {
    setMessages([])
  }

  return { messages, resetMessages, addMessage }
}
